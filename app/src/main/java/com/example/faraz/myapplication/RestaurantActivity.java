package com.example.faraz.myapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.faraz.myapplication.Restaurant_JsonData.RestaurantAdapter;
import com.example.faraz.myapplication.Restaurant_JsonData.RestaurantModel;
import com.example.faraz.myapplication.Utills.ConfigURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RestaurantActivity extends AppCompatActivity {

    String data,json_val,r_name,r_id;
    StringBuilder stringBuilder;
    JSONObject jobject;
    JSONArray jarray;
    ListView lv;
    List<RestaurantModel> restaurantList;
    RestaurantAdapter restaurantAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        Background bg = new Background();
        bg.execute();

        lv = findViewById(R.id.listshow);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String id_val = ((TextView) view.findViewById(R.id.res_id)).getText().toString();
                Intent menupage = new Intent(RestaurantActivity.this, MenuActivity.class);
                menupage.putExtra("rest_id",id_val);
                startActivity(menupage);
            }
        });

    }



    class Background extends AsyncTask<String,Object,List<RestaurantModel>>
    {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected List<RestaurantModel> doInBackground(String... strings) {

            try {

                fetching(ConfigURL.URL_GET_RESTAURANT_LIST);
                json_val = String.valueOf(stringBuilder);
                conversion(json_val);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return restaurantList;
        }


        @Override
        protected void onPostExecute(List<RestaurantModel> restaurants) {

             restaurantAdapter = new RestaurantAdapter(getApplicationContext(),R.layout.res_layout,restaurants);
             lv.setAdapter(restaurantAdapter);
        }

    }



    //FOR FETCHING DATA FROM SERVER AND BRING THE JSON IN stringBuilder VARIABLE

    public String fetching(String JSON) throws IOException {



        URL url = new URL(JSON);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        InputStream inputStream = urlConnection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        stringBuilder = new StringBuilder();

        while ((data = reader.readLine()) != null)
        {
            stringBuilder.append(data+"\n");
        }

        reader.close();
        inputStream.close();
        urlConnection.disconnect();

        return stringBuilder.toString().trim();
    }




    public List<RestaurantModel> conversion(String json_data) throws JSONException {

        lv = findViewById(R.id.listshow);
        restaurantList = new ArrayList<>();
        jobject = new JSONObject(json_data);
        jarray = jobject.getJSONArray("restaurant");
        int count = 0;

        while(count < jarray.length())
        {
            JSONObject object = jarray.getJSONObject(count);

            r_id = object.getString("res_id");
            r_name = object.getString("res_name");

            RestaurantModel res = new RestaurantModel(r_id,r_name);
            restaurantList.add(res);
            count++;

        }

        return restaurantList;
    }

}
