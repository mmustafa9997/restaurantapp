package com.example.faraz.myapplication.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Adapters.AdapterDeals;
import com.example.faraz.myapplication.Adapters.AdapterItems;
import com.example.faraz.myapplication.BlankFragment;
import com.example.faraz.myapplication.Models.Deals;
import com.example.faraz.myapplication.Models.Items;
import com.example.faraz.myapplication.R;
import com.example.faraz.myapplication.Utills.ConfigURL;
import com.example.faraz.myapplication.Utills.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class DealsAndItemFragment extends Fragment implements View.OnClickListener {


    ArrayList<Deals> data = new ArrayList<>();
    ArrayList<Items> itemsArrayList = new ArrayList<>();
    RecyclerView rv, rv1;
    String get_mall_id;
    int randomNumber;
    String order_id;
    Button btn_proceed_order;

    public DealsAndItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_deals, container, false);
        btn_proceed_order = rootView.findViewById(R.id.btn_proceed_order);
        rv = (RecyclerView) rootView.findViewById(R.id.rv_deals);
        rv.setHasFixedSize(true);


        rv1 = (RecyclerView) rootView.findViewById(R.id.rv_items);
        rv1.setHasFixedSize(true);


        get_mall_id = getArguments().getString("res_id");

        if (get_mall_id != null) {

            Random r = new Random();
            randomNumber = r.nextInt(10000000);
            Log.d("RAN", "" + randomNumber);
            ProgressDialogClass.showProgress(getActivity());

            getNearestMall(get_mall_id);

            getItems(get_mall_id);

            createOrder();
        }

        btn_proceed_order.setOnClickListener(this);
        return rootView;
    }

    //Get Deals
    public void getNearestMall(String mall_id) {

        data.clear();
        AndroidNetworking.get(ConfigURL.URL_GET_DEALS)
                .addQueryParameter("resId", mall_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        ProgressDialogClass.hideProgress();


                        String deal_id, deal_name, deal_price, deal_description, deal_image, deal_res_name;
                        try {
                            JSONArray jsonArray = response.getJSONArray("deals");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                Deals deals = new Deals(jsonArray.getJSONObject(i));
                                deal_id = deals.getDeal_id();
                                deal_name = deals.getDeal_name();
                                deal_description = deals.getDeal_description();
                                deal_image = deals.getDeal_images();
                                deal_price = deals.getDeal_price();
                                deal_res_name = deals.getDeal_restaurant_name();


                                Deals deals1 = new Deals(deal_id, deal_name, deal_price, deal_description, deal_image, deal_res_name, randomNumber, order_id);
                                data.add(deals1);


                                AdapterDeals adapter = new AdapterDeals(getActivity(), data);
                                rv.setAdapter(adapter);


                                LinearLayoutManager llm
                                        = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true);

                                llm.setReverseLayout(true);
                                llm.setStackFromEnd(true);

                                rv.setLayoutManager(llm);
                                rv.setItemAnimator(new DefaultItemAnimator());

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.hideProgress();

                    }
                });
    }

    //Getting Order Id From Server
    public void createOrder() {
        // ProgressDialogClass.showProgress(getActivity());
        //Toast.makeText(this,""+username+""+password,Toast.LENGTH_LONG).show();
        AndroidNetworking.post(ConfigURL.URL_POST_CREATE_ORDERS)
                .addBodyParameter("cust_name", ConfigURL.getLoginUserName(getActivity()))
                .addBodyParameter("res_id", get_mall_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //ProgressDialogClass.hideProgress();
                        try {
                            JSONArray jsonArray = response.getJSONArray("result");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject json_data = jsonArray.getJSONObject(i);

                                order_id = json_data.getString("orderId");
                                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("ORDERID", order_id);
                                editor.commit();
                                //Toast.makeText(getActivity(), order_id, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        //ProgressDialogClass.hideProgress();
                    }
                });
    }

    //Get Items
    public void getItems(String mall_id) {

        data.clear();
        AndroidNetworking.get(ConfigURL.URL_GET_ITEMS)
                .addQueryParameter("resId", mall_id)
                .addQueryParameter("cust_name", ConfigURL.getLoginUserName(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Toast.makeText(getActivity(), response + "", Toast.LENGTH_LONG).show();

                        String deal_id, deal_name, deal_price, deal_description, deal_image, deal_res_name;
                        try {
                            JSONArray jsonArray = response.getJSONArray("items");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                Items deals = new Items(jsonArray.getJSONObject(i));
                                deal_id = deals.getItem_id();
                                deal_name = deals.getItem_name();
                                deal_description = deals.getItem_description();
                                deal_image = deals.getItem_images();
                                deal_price = deals.getItem_price();
                                deal_res_name = deals.getItem_res_name();

                                Items deals1 = new Items(deal_id, deal_name, deal_price, deal_description, deal_image, deal_res_name, randomNumber, order_id);
                                itemsArrayList.add(deals1);


                                AdapterItems adapter = new AdapterItems(getActivity(), itemsArrayList);
                                rv1.setAdapter(adapter);

                                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                                rv1.setLayoutManager(llm);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void confirmOrder() {
        ProgressDialogClass.showProgress(getActivity());
        AndroidNetworking.post(ConfigURL.URL_POST_PROCEED_ORDERS)
                .addBodyParameter("cust_name", ConfigURL.getLoginUserName(getActivity()))
                .addBodyParameter("order_id", ConfigURL.getOrderId(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getActivity(), "Order Proceed", Toast.LENGTH_LONG).show();
                        Fragment fragmentName = null;
                        Fragment BlankFragment = new BlankFragment();

                        fragmentName = BlankFragment;
                        replaceFragment(fragmentName);
                        ProgressDialogClass.hideProgress();
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.hideProgress();
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_proceed_order:
                confirmOrder();
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;


        FragmentManager manager = getFragmentManager();
        boolean fragmentPopped = false;
        try {

            fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        } catch (IllegalStateException ignored) {
            // There's no way to avoid getting this if saveInstanceState has already been called.
        }
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commitAllowingStateLoss();
        }

    }


    /*public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }*/

}