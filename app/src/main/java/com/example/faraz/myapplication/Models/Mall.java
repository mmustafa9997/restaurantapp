package com.example.faraz.myapplication.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Maryam on 6/30/2018.
 */

public class Mall {
    String mall_id ;
    String mall_name ;
    String mall_distance ;
    String mall_area_id ;

    public Mall(String mall_id, String mall_name, String mall_distance, String mall_area_id) {
        this.mall_id = mall_id;
        this.mall_name = mall_name;
        this.mall_distance = mall_distance;
        this.mall_area_id = mall_area_id;
    }

    public String getMall_id() {
        return mall_id;
    }

    public void setMall_id(String mall_id) {
        this.mall_id = mall_id;
    }

    public String getMall_name() {
        return mall_name;
    }

    public void setMall_name(String mall_name) {
        this.mall_name = mall_name;
    }

    public String getMall_distance() {
        return mall_distance;
    }

    public void setMall_distance(String mall_distance) {
        this.mall_distance = mall_distance;
    }

    public String getMall_area_id() {
        return mall_area_id;
    }

    public void setMall_area_id(String mall_area_id) {
        this.mall_area_id = mall_area_id;
    }

    public Mall(JSONObject jsonObject) {
        try {
            this.mall_id = jsonObject.getString("mallid");
            this.mall_name = jsonObject.getString("mallName");
            this.mall_distance = jsonObject.getString("distance");
            this.mall_area_id = jsonObject.getString("mallAreaId");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
