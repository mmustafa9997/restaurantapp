package com.example.faraz.myapplication.Utills;

import android.content.Context;

import com.example.faraz.myapplication.R;

import dmax.dialog.SpotsDialog;

public class ProgressDialogClass {

    public static SpotsDialog progressDialog ;

    public static void showProgress(Context context)
    {
        progressDialog = new SpotsDialog(context, R.style.CustomProgressDialog);
        progressDialog.show();
    }

    public static void hideProgress()
    {
        progressDialog.dismiss();
    }
}