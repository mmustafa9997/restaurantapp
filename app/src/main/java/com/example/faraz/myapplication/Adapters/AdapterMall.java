package com.example.faraz.myapplication.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.faraz.myapplication.DrawerActivity;
import com.example.faraz.myapplication.Models.Mall;
import com.example.faraz.myapplication.Fragments.RestaurantFragment;
import com.example.faraz.myapplication.R;

import java.util.ArrayList;

public class
AdapterMall extends RecyclerView.Adapter<AdapterMall.MyViewHolder> {

    private Context acontext;
    private ArrayList<Mall> arrayList;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CardView mCardView;
        String mall_id ;
        public TextView tv_pending_order_nearest_mall;

        public MyViewHolder(View v) {
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view_mall);
            tv_pending_order_nearest_mall = (TextView) v.findViewById(R.id.tv_restaurant_inside_mall);


            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Mall current = arrayList.get(getLayoutPosition());
            mall_id = current.getMall_area_id();


            Fragment fragmentName = null;
            Fragment RestaurantFragmentOpen = new RestaurantFragment();
            fragmentName = RestaurantFragmentOpen;

            Bundle args = new Bundle();

            args.putString("mall_id", ""+mall_id);
            fragmentName.setArguments(args);

            replaceFragment(fragmentName);
        }
    }

    public AdapterMall(Context context, ArrayList<Mall> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterMall.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_nearest_mall, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Mall current = arrayList.get(position);
        holder.tv_pending_order_nearest_mall.setText(current.getMall_name());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        DrawerActivity myActivity = (DrawerActivity)acontext;

        FragmentManager manager = myActivity.getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

}