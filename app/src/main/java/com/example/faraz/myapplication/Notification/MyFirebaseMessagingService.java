package com.example.faraz.myapplication.Notification;

import android.app.NotificationManager;
import android.content.Context;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.faraz.myapplication.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCM Service";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getData().toString());
        try {
            JSONObject notificationObject = new JSONObject(remoteMessage.getData().toString());
            addNotification(notificationObject.getJSONObject("data"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void addNotification(JSONObject notificationObject) throws JSONException {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("" + notificationObject.getString("resName"))
                        .setContentText("" + notificationObject.getString("message"));

       /* String latitude = notificationObject.getString("latitude");
        String longitude = notificationObject.getString("longitude");
        String title = notificationObject.getString("title");
        String address = notificationObject.getString("address");
        String jobId = notificationObject.getString("jobId");
        String type = notificationObject.getString("type");
        String customerMobileNumber = notificationObject.getString("customerMobileNo");*/


        /*Intent notificationIntent = new Intent(this, DrawerMainActivity.class);
        notificationIntent.putExtra("latitude", latitude);
        notificationIntent.putExtra("longitude", longitude);
        notificationIntent.putExtra("title", title);
        notificationIntent.putExtra("address", address);
        notificationIntent.putExtra("type", type);
        notificationIntent.putExtra("jobId", jobId);
        notificationIntent.putExtra("customerMobileNumber",customerMobileNumber);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);*/

        builder.setSound(Settings.System.DEFAULT_RINGTONE_URI);
//        builder.setColor(Integer.parseInt(remoteMessage.getNotification().getColor()));
        // Add as notification


        final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());


      /*  Intent noIntent = new Intent(this, DrawerMainActivity.class);
        noIntent.putExtra("latitude", latitude);
        noIntent.putExtra("longitude", longitude);
        noIntent.putExtra("title", title);
        noIntent.putExtra("address", address);
        noIntent.putExtra("type", type);
        noIntent.putExtra("jobId", jobId);
        noIntent.putExtra("customerMobileNumber", customerMobileNumber);
        noIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(noIntent);*/
/*

        Log.d(TAG, "Body " + "Lat : " + latitude + " Lng : " + longitude + " Job Title : " + title + " Address : " + address + " Notification Type : " + type + " Customer Mobile : " + customerMobileNumber + "Job Id :" + jobId);
*/


    }
}
