package com.example.faraz.myapplication.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Maryam on 7/1/2018.
 */

public class Deals {
    String deal_id;
    String deal_name;
    String deal_price;
    String deal_description;
    String deal_images;
    String deal_restaurant_name;
    int deal_booking_id;
    public int quantity;
    String order_id ;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public int getDeal_booking_id() {
        return deal_booking_id;
    }

    public void setDeal_booking_id(int deal_booking_id) {
        this.deal_booking_id = deal_booking_id;
    }

    public Deals(String deal_id, String deal_name, String deal_price, String deal_description, String deal_images, String deal_restaurant_name, int deal_booking_id , String order_id) {
        this.deal_id = deal_id;
        this.deal_name = deal_name;
        this.deal_price = deal_price;
        this.deal_description = deal_description;
        this.deal_images = deal_images;
        this.deal_restaurant_name = deal_restaurant_name;
        this.deal_booking_id = deal_booking_id;
        this.order_id = order_id ;
        quantity = 0;
    }

    public String getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(String deal_id) {
        this.deal_id = deal_id;
    }

    public String getDeal_name() {
        return deal_name;
    }

    public void setDeal_name(String deal_name) {
        this.deal_name = deal_name;
    }

    public String getDeal_price() {
        return deal_price;
    }

    public void setDeal_price(String deal_price) {
        this.deal_price = deal_price;
    }

    public String getDeal_description() {
        return deal_description;
    }

    public void setDeal_description(String deal_description) {
        this.deal_description = deal_description;
    }

    public String getDeal_images() {
        return deal_images;
    }

    public void setDeal_images(String deal_images) {
        this.deal_images = deal_images;
    }

    public String getDeal_restaurant_name() {
        return deal_restaurant_name;
    }

    public void setDeal_restaurant_name(String deal_restaurant_name) {
        this.deal_restaurant_name = deal_restaurant_name;
    }


    public Deals(JSONObject jsonObject) {
        try {
            this.deal_id = jsonObject.getString("dealId");
            this.deal_name = jsonObject.getString("dealName");
            this.deal_price = jsonObject.getString("dealPrice");
            this.deal_description = jsonObject.getString("dealDescription");
            this.deal_images = jsonObject.getString("dealImage");
            this.deal_restaurant_name = jsonObject.getString("restaurantName");
            //this.res_image = jsonObject.getString("mallAreaId");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
