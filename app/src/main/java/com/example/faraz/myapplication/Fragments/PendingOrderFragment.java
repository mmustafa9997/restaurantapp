package com.example.faraz.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Adapters.AdapterOrderPending;
import com.example.faraz.myapplication.Models.PendingOrders;
import com.example.faraz.myapplication.R;
import com.example.faraz.myapplication.Utills.ConfigURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PendingOrderFragment extends Fragment {


    ArrayList<PendingOrders> data = new ArrayList<>();
    RecyclerView rv;

    public PendingOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_pending, container, false);
        rv = (RecyclerView) rootView.findViewById(R.id.rv_order_pending);
        rv.setHasFixedSize(true);

        //loadData();
        getItems();

        return rootView;
    }

    public void loadData() {

        data.clear();

        PendingOrders completedOrders = new PendingOrders("Millennium Mall", "Confirmed", "Food Court");
        data.add(completedOrders);

        AdapterOrderPending adapter = new AdapterOrderPending(getActivity(), data);
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
    }

    public void getItems() {

        data.clear();
        AndroidNetworking.get(ConfigURL.URL_GET_CONFIRMED_ORDERS)
                .addQueryParameter("cust_name", ConfigURL.getLoginUserName(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Toast.makeText(getActivity(), response + "", Toast.LENGTH_LONG).show();

                        String restaturant, mall_name, status;
                        try {
                            JSONArray jsonArray = response.getJSONArray("items");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                PendingOrders deals = new PendingOrders(jsonArray.getJSONObject(i));
                                restaturant = deals.getOrder_restaurant();
                                mall_name = deals.getNearest_mall();
                                status = deals.getOrder_status();

                                PendingOrders deals1 = new PendingOrders(mall_name, status, restaturant);
                                data.add(deals1);


                                AdapterOrderPending adapter = new AdapterOrderPending(getActivity(), data);
                                rv.setAdapter(adapter);

                                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                                rv.setLayoutManager(llm);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}