package com.example.faraz.myapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.faraz.myapplication.Models.PendingOrders;
import com.example.faraz.myapplication.R;

import java.util.ArrayList;

public class AdapterOrderPending extends RecyclerView.Adapter<AdapterOrderPending.MyViewHolder> {

    private Context acontext;
    private ArrayList<PendingOrders> arrayList ;

    public  class MyViewHolder extends RecyclerView.ViewHolder{

        public CardView mCardView;
        public TextView tv_pending_order_restaurant, tv_pending_order_nearest_mall, tv_pending_order_status;

        public MyViewHolder(View v){
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view_order_pending);
            tv_pending_order_restaurant = (TextView) v.findViewById(R.id.tv_pending_order_restaurant);
            tv_pending_order_nearest_mall = (TextView) v.findViewById(R.id.tv_restaurant_inside_mall);
            tv_pending_order_status = (TextView) v.findViewById(R.id.tv_pending_order_status);
        }

    }
    public AdapterOrderPending(Context context, ArrayList<PendingOrders> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterOrderPending.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_order_pending, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        PendingOrders current = arrayList.get(position);
        holder.tv_pending_order_restaurant.setText(current.getOrder_restaurant());
        holder.tv_pending_order_nearest_mall.setText(current.getNearest_mall());
        holder.tv_pending_order_status.setText(current.getOrder_status());
    }

    @Override
    public int getItemCount() {return arrayList.size(); }

}