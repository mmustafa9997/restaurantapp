package com.example.faraz.myapplication.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.faraz.myapplication.R;
import com.example.faraz.myapplication.Utills.ProgressDialogClass;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Mapfragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {

    Button btn_find_restaurant;
    //For Google Map
    MapView mMapView;
    GoogleMap mGoogleMap;
    Location mLocation;
    private View rootView;
    public Mapfragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.map_fragment, container, false);
        btn_find_restaurant = rootView.findViewById(R.id.btn_find_restaurant);
        btn_find_restaurant.setOnClickListener(this);
        mMapView = (MapView) rootView.findViewById(R.id.map);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        ProgressDialogClass.showProgress(getActivity());

        mGoogleMap = googleMap;
        MapsInitializer.initialize(getContext());
        //mGoogleMap.setMyLocationEnabled(true);

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria crit = new Criteria();

        mLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(crit, false));

        LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
        mGoogleMap.animateCamera(cameraUpdate);

        mGoogleMap.addMarker(new MarkerOptions()
                .position(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()))
                .title("You Are Here"));

        ProgressDialogClass.hideProgress();


        /*mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapClick(LatLng latLng) {
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                Criteria crit = new Criteria();

                mLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(crit, false));

                latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 14);
                mGoogleMap.animateCamera(cameraUpdate);
            }
        });*/

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_find_restaurant:

                ProgressDialogClass.showProgress(getActivity());

                Fragment fragmentName = null;
                Fragment RestaurantFragment = new MallFragment();

                Bundle bundle = new Bundle();
                bundle.putString("lat", "" + mLocation.getLatitude());
                bundle.putString("lng", "" + mLocation.getLongitude());
                RestaurantFragment.setArguments(bundle);

                fragmentName = RestaurantFragment;
                replaceFragment(fragmentName);

                ProgressDialogClass.hideProgress();

                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;


        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = false;
        try {

            fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        } catch (IllegalStateException ignored) {
            // There's no way to avoid getting this if saveInstanceState has already been called.
        }
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commitAllowingStateLoss();
        }

    }

}