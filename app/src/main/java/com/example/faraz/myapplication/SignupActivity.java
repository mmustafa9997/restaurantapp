package com.example.faraz.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Utills.ConfigURL;
import com.example.faraz.myapplication.Utills.ProgressDialogClass;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends AppCompatActivity {

    EditText Username, Email, Password;
    Button SignupBtn;
    TextView Login;
    String user, email, pass, response, line, answer;
    boolean error = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Username = (EditText) findViewById(R.id.username);
        Email = (EditText) findViewById(R.id.email);
        Password = (EditText) findViewById(R.id.password);
        Login = (TextView) findViewById(R.id.loginText);
        SignupBtn = (Button) findViewById(R.id.signup);

        SignupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user = Username.getText().toString();
                email = Email.getText().toString();
                pass = Password.getText().toString();

                if (user.length() == 0 || email.length() == 0 || pass.length() == 0) {
                    Toast T1 = Toast.makeText(getApplicationContext(), "FILL ALL FIELDS FIRST", Toast.LENGTH_LONG);
                    T1.show();
                } else {
                    sendData();
                }
            }
        });


        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent login = new Intent(SignupActivity.this, LoginActivity.class);
                startActivity(login);
            }
        });

    }


    public void sendData() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Toast.makeText(this,""+username+""+password,Toast.LENGTH_LONG).show();
        AndroidNetworking.post(ConfigURL.URL_REGISTRATION)
                .addBodyParameter("cust_username", user)
                .addBodyParameter("cust_email", email)
                .addBodyParameter("cust_password", pass)
                .addBodyParameter("cust_fcm", refreshedToken)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();

                        try {
                            String msg = response.getString("message");
                            //Toast.makeText(SignupActivity.this, "Registration Successful", Toast.LENGTH_LONG).show();
                            error = response.getBoolean("error");

                            if (!error) {
                                Toast.makeText(SignupActivity.this, "Registration Successful", Toast.LENGTH_LONG).show();
                                SharedPreferences preferences = getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("USER", user);
                                editor.commit();
                                //NEW ACTIVITY
                                Intent restaurant = new Intent(SignupActivity.this, DrawerActivity.class);
                                startActivity(restaurant);
                            } else {
                                Toast.makeText(SignupActivity.this, "Registration Failed", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        ProgressDialogClass.hideProgress();
                        Toast.makeText(SignupActivity.this, "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }


/*    public class Connection extends AsyncTask {

        AlertDialog alertDialog;

        @Override
        protected void onPreExecute() {

            alertDialog = new AlertDialog.Builder(SignupActivity.this).create();
            alertDialog.setTitle("SIGNING UP..!!");
        }

        @Override
        protected Object doInBackground(Object[] param) {
            try {

                answer = Register(ConfigURL.URL_REGISTRATION);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {

            if (answer.equals("DONE")) {
                alertDialog.setMessage("SIGNUP SUCCESFUL..");
                alertDialog.show();
            } else {
                alertDialog.setMessage("USERNAME OR EMAIL ALREADY EXISTED");
                alertDialog.show();
            }
        }
    }


    public String Register(String api) throws IOException {

        String values = URLEncoder.encode("c_user", "UTF-8") + "=" + URLEncoder.encode(user, "UTF-8") + "&" +
                URLEncoder.encode("c_email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&" +
                URLEncoder.encode("c_password", "UTF-8") + "=" + URLEncoder.encode(pass, "UTF-8");

        URL url = new URL(api);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        OutputStream os = connection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(values);
        writer.flush();
        writer.close();
        os.close();

        InputStream inputStream = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
        response = "";
        line = "";

        while ((line = reader.readLine()) != null) {
            response += line;
        }
        reader.close();
        inputStream.close();
        connection.disconnect();

        return response;
    }*/

}