package com.example.faraz.myapplication.Utills;

import android.content.Context;
import android.content.SharedPreferences;

public class ConfigURL {
    public static final String URL_LOGIN = "http://www.itehadmotors.com/Restaurant/v1/login/customer";
    public static final String URL_REGISTRATION = "http://www.itehadmotors.com/Restaurant/v1/customerregister";
    public static final String URL_GET_RESTAURANT_LIST = "https://retial-profits.000webhostapp.com/html/customer_actions/restaurant_data_json.php";
    public static final String URL_GET_MENU_ITEM = "https://retial-profits.000webhostapp.com/html/customer_actions/menu_data_json.php";
    public static final String URL_GET_NEAREST_MALL = "http://www.itehadmotors.com/Restaurant/v1/listofnearestmall";
    public static final String URL_GET_RESTAURANT = "http://www.itehadmotors.com/Restaurant/v1/listofrestaurant";
    public static final String URL_GET_DEALS = "http://www.itehadmotors.com/Restaurant/v1/dealsofrestaurant";
    public static final String URL_GET_ITEMS = "http://www.itehadmotors.com/Restaurant/v1/itemsofrestaurant";
    public static final String URL_POST_CART = "http://www.itehadmotors.com/Restaurant/v1/addtocart";
    public static final String URL_POST_UPDATE_CART = "http://www.itehadmotors.com/Restaurant/v1/updatecart";
    public static final String URL_UPDATE_FCM_KEY = "http://www.itehadmotors.com/Restaurant/v1/insertfcmcustomer";
    public static final String URL_GET_COMPLETED_ORDERS = "http://www.itehadmotors.com/Restaurant/v1/customercompletedorder";
    public static final String URL_GET_CONFIRMED_ORDERS = "http://www.itehadmotors.com/Restaurant/v1/customerconfirmedorder";
    public static final String URL_POST_CREATE_ORDERS = "http://www.itehadmotors.com/Restaurant/v1/createorder";
    public static final String URL_POST_PROCEED_ORDERS = "http://www.itehadmotors.com/Restaurant/v1/proceedorder";

    public static String getLoginState(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("USER", "").length() > 0) {
            return prefs.getString("USER", "");
        } else
            return "";
    }

    public static String getLoginUserName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("USER", "").length() > 0) {
            return prefs.getString("USER", "");
        } else
            return "";
    }

    public static String getOrderId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("ORDERID", "").length() > 0) {
            return prefs.getString("ORDERID", "");
        } else
            return "";
    }

    public static void clearshareprefrence(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
