package com.example.faraz.myapplication.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.faraz.myapplication.DrawerActivity;
import com.example.faraz.myapplication.Fragments.DealsAndItemFragment;
import com.example.faraz.myapplication.Fragments.RestaurantFragment;
import com.example.faraz.myapplication.Models.Deals;
import com.example.faraz.myapplication.Models.Mall;
import com.example.faraz.myapplication.Models.Restaurant;
import com.example.faraz.myapplication.R;

import java.util.ArrayList;

public class
AdapterRestaurant extends RecyclerView.Adapter<AdapterRestaurant.MyViewHolder> {

    private Context acontext;
    private ArrayList<Restaurant> arrayList;
    String res_id;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView mCardView;
        public TextView tv_restaurant_inside_mall, tv_mall_name;

        public MyViewHolder(View v) {
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view_restaurant_inside_mall);
            tv_restaurant_inside_mall = (TextView) v.findViewById(R.id.tv_restaurant_inside_mall);
            tv_mall_name = (TextView) v.findViewById(R.id.tv_mall_name);


            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Restaurant current = arrayList.get(getLayoutPosition());
                    res_id = current.getRes_id();

                    Log.d("RESID", "" + res_id);
                    Fragment fragmentName = null;
                    Fragment OpenDealsAndItemFragment = new DealsAndItemFragment();
                    fragmentName = OpenDealsAndItemFragment;

                    Bundle args = new Bundle();

                    args.putString("res_id", "" + res_id);
                    fragmentName.setArguments(args);

                    replaceFragment(fragmentName);

                }
            });
        }

    }

    public AdapterRestaurant(Context context, ArrayList<Restaurant> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterRestaurant.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_restaurant_inside_mall, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Restaurant current = arrayList.get(position);
        holder.tv_restaurant_inside_mall.setText(current.getRes_name());
        holder.tv_mall_name.setText(current.getRes_mall_name());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        DrawerActivity myActivity = (DrawerActivity) acontext;

        FragmentManager manager = myActivity.getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

}