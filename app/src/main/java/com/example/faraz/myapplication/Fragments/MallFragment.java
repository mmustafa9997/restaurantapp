package com.example.faraz.myapplication.Fragments;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Models.Mall;
import com.example.faraz.myapplication.Adapters.AdapterMall;
import com.example.faraz.myapplication.R;
import com.example.faraz.myapplication.Utills.ConfigURL;
import com.example.faraz.myapplication.Utills.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MallFragment extends Fragment   {


    ArrayList<Mall> data = new ArrayList<>();
    RecyclerView rv;
    Double lat, lng;

    public MallFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_mall, container, false);
        rv = (RecyclerView) rootView.findViewById(R.id.rv_mall);
        rv.setHasFixedSize(true);

        lat = Double.valueOf(getArguments().getString("lat"));
        lng = Double.valueOf(getArguments().getString("lng"));

        if (lat != null && lng != null) {

            ProgressDialogClass.showProgress(getActivity());

            getNearestMall(lat, lng);
        }
        return rootView;
    }

    public void getNearestMall(Double latitude, Double longitude) {

        data.clear();
        AndroidNetworking.get(ConfigURL.URL_GET_NEAREST_MALL)
                .addQueryParameter("latitude", latitude.toString())
                .addQueryParameter("longitude", longitude.toString())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        ProgressDialogClass.hideProgress();


                        String mall_id, mall_name, mall_area_id, mall_distance;
                        try {
                            JSONArray jsonArray = response.getJSONArray("mall");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                Mall mall = new Mall(jsonArray.getJSONObject(i));

                                mall_id = mall.getMall_id();
                                mall_name = mall.getMall_name();
                                mall_distance = mall.getMall_distance();
                                mall_area_id = mall.getMall_area_id();
                                Mall mall1 = new Mall(mall_id, mall_name, mall_distance, mall_area_id);
                                data.add(mall1);

//                                if(i==0)
//                                    customNotification(mall_name);


                                AdapterMall adapter = new AdapterMall(getActivity(), data);
                                rv.setAdapter(adapter);

                                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                                rv.setLayoutManager(llm);


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.hideProgress();

                    }
                });
    }

    public void customNotification(String mall_nmae) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(getActivity())
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle("Nearest Mall Find")
                        .setContentText(mall_nmae);
        final NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(111, builder.build());

    }

/*

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onClick(View view, int position) {

        Fragment fragmentName = null;
        Fragment RestaurantFragmentOpen = new RestaurantFragment();
        fragmentName = RestaurantFragmentOpen;

        Bundle args = new Bundle();

        args.putString("mall_id", ""+data.get(position).getMall_area_id());
        fragmentName.setArguments(args);

        replaceFragment(fragmentName);
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }*/

}