package com.example.faraz.myapplication.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Maryam on 6/30/2018.
 */

public class CompletedOrders {
    String restaurant;
    String order_mall ;
    String area;
    String order_price ;

    public CompletedOrders(String restaurant, String order_mall, String area, String order_price) {
        this.restaurant = restaurant;
        this.order_mall = order_mall;
        this.area = area;
        this.order_price = order_price;
    }

    public CompletedOrders(JSONObject jsonObject) {
        try {
            this.restaurant = jsonObject.getString("restaurantName");
            this.area = jsonObject.getString("areaName");
            this.order_price = jsonObject.getString("price");
            this.order_mall = jsonObject.getString("mallName");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(String restaurant) {
        this.restaurant = restaurant;
    }

    public String getOrder_mall() {
        return order_mall;
    }

    public void setOrder_mall(String order_mall) {
        this.order_mall = order_mall;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getOrder_price() {
        return order_price;
    }

    public void setOrder_price(String order_price) {
        this.order_price = order_price;
    }
}
