package com.example.faraz.myapplication.Menu_JsonData;

public class Menu {

    private String item_name, item_img;
    private String item_price,item_id;


    public Menu(String id, String name, String price, String img)
    {
        setItem_name(name);
        setItem_price(price);
        setItem_img(img);
        setItem_id(id);
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_img() {
        return item_img;
    }

    public void setItem_img(String item_img) {
        this.item_img = item_img;
    }

    public String getItem_price() {
        return item_price;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }
}
