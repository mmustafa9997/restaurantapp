package com.example.faraz.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Utills.ConfigURL;
import com.example.faraz.myapplication.Utills.ProgressDialogClass;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText user, pass;
    TextView signup;
    Button loginbtn;
    String username, password, response, line;
    boolean error = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (ConfigURL.getLoginState(this).length() > 0) {
            Intent intent = new Intent(this, DrawerActivity.class);
            startActivity(intent);
            finish();
        }


        user = (EditText) findViewById(R.id.username);
        pass = (EditText) findViewById(R.id.password);
        signup = (TextView) findViewById(R.id.SignupText);
        loginbtn = (Button) findViewById(R.id.login);

        loginbtn.setOnClickListener(this);
        signup.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.login:
                username = user.getText().toString();
                password = pass.getText().toString();

                if (username.length() == 0 || password.length() == 0) {
                    Toast t = Toast.makeText(getApplicationContext(), "FILL ALL FIELDS FIRST", Toast.LENGTH_LONG);
                    t.show();
                } else {
                    ProgressDialogClass.showProgress(this);
                    /*Connect con = new Connect();
                    con.execute();*/
                    sendData();
                }
                break;
            case R.id.SignupText:
                Intent sign = new Intent(LoginActivity.this, SignupActivity.class);
                startActivity(sign);
                break;
        }
    }


    public void sendData() {

        //Toast.makeText(this,""+username+""+password,Toast.LENGTH_LONG).show();
        AndroidNetworking.post(ConfigURL.URL_LOGIN)
                .addBodyParameter("cust_username", username)
                .addBodyParameter("cust_password", password)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.hideProgress();

                        try {
                            String msg = response.getString("message");
                            Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
                            error = response.getBoolean("error");

                            if (!error) {
                                updateFCMKey();
                                SharedPreferences preferences = getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("USER", username);
                                editor.commit();
                                //NEW ACTIVITY
                                Intent restaurant = new Intent(LoginActivity.this, DrawerActivity.class);
                                startActivity(restaurant);
                            } else {
                                Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        ProgressDialogClass.hideProgress();
                        Toast.makeText(LoginActivity.this, "" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }


    public void updateFCMKey() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Toast.makeText(this,""+username+""+password,Toast.LENGTH_LONG).show();
        AndroidNetworking.post(ConfigURL.URL_UPDATE_FCM_KEY)
                .addBodyParameter("cust_name", username)
                .addBodyParameter("cust_fcm", refreshedToken)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }

                    @Override
                    public void onError(ANError error) {
                    }
                });
    }

/*    public class Connect extends AsyncTask {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Object doInBackground(Object[] param) {
            try {
                Login(ConfigURL.URL_LOGIN);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Object o) {

            if (response.equals("LOGIN FAILED")) {
                Toast.makeText(LoginActivity.this, "LOGIN FAILED TRY AGAIN", Toast.LENGTH_LONG).show();
            } else {

                SharedPreferences preferences = getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("USER", username);
                editor.commit();
                //NEW ACTIVITY
                Intent restaurant = new Intent(LoginActivity.this, DrawerActivity.class);
                startActivity(restaurant);

            }

        }
    }

    public String Login(String api) throws IOException {

        String values = URLEncoder.encode("user_name", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8") + "&" +
                URLEncoder.encode("pass", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");

        URL url = new URL(api);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        OutputStream os = connection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(values);
        writer.flush();
        writer.close();
        os.close();

        InputStream inputStream = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
        response = "";
        line = "";

        while ((line = reader.readLine()) != null) {
            response += line;
        }
        reader.close();
        inputStream.close();
        connection.disconnect();

        return response;

    }*/

}
