package com.example.faraz.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Adapters.AdapterOrderCompleted;
import com.example.faraz.myapplication.Models.CompletedOrders;
import com.example.faraz.myapplication.R;
import com.example.faraz.myapplication.Utills.ConfigURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CompleteOrderFragment extends Fragment {


    ArrayList<CompletedOrders> data = new ArrayList<>();
    RecyclerView rv;

    public CompleteOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_three, container, false);
        rv = (RecyclerView) rootView.findViewById(R.id.rv_order_cancel);
        rv.setHasFixedSize(true);

        loadData();
        getItems();

        return rootView;
    }

    public void loadData() {

        CompletedOrders completedOrders = new CompletedOrders("Gulistan-e-Johar, Karachi", "Millennium Mall", "B.B.Q", "Rs. 995");
        data.add(completedOrders);
        CompletedOrders completedOrders1 = new CompletedOrders("Block 5 Gulistan-e-Johar, Karachi", "City Towers & Shopping Mall", "Fast Food", "750");
        data.add(completedOrders1);
        CompletedOrders completedOrders2 = new CompletedOrders("Sharah-e-Sher Shah Suri, Block C North Nazimabad, Karachi", "Dolmen Mall Hyderi", "Sea Food", "320");
        data.add(completedOrders2);
        CompletedOrders completedOrders3 = new CompletedOrders(" LA-2/B, Block 21, Opp. UBL Sports Complex", "LuckyOne Mall", "Street Food", "1250");
        data.add(completedOrders3);

        AdapterOrderCompleted adapter = new AdapterOrderCompleted(getActivity(), data);
        rv.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
    }

    public void getItems() {

        data.clear();
        AndroidNetworking.get(ConfigURL.URL_GET_COMPLETED_ORDERS)
                .addQueryParameter("cust_name", ConfigURL.getLoginUserName(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Toast.makeText(getActivity(), response + "", Toast.LENGTH_LONG).show();

                        String restaturant, mall_name, aera, price;
                        try {
                            JSONArray jsonArray = response.getJSONArray("items");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                CompletedOrders deals = new CompletedOrders(jsonArray.getJSONObject(i));
                                restaturant = deals.getRestaurant();
                                mall_name = deals.getOrder_mall();
                                aera = deals.getArea();
                                price = deals.getOrder_price();

                                CompletedOrders deals1 = new CompletedOrders(restaturant, mall_name, aera, price);
                                data.add(deals1);


                                AdapterOrderCompleted adapter = new AdapterOrderCompleted(getActivity(), data);
                                rv.setAdapter(adapter);

                                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                                rv.setLayoutManager(llm);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}