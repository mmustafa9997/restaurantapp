package com.example.faraz.myapplication.Utills;

import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Fragments.MallFragment;
import com.example.faraz.myapplication.Models.Mall;
import com.example.faraz.myapplication.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class CurrentLocation extends Fragment {

    public Criteria criteria;
    public String bestProvider;
    public double latitude;
    public double longitude;


    String address = "";
    Geocoder geocoder;
    List<Address> addresses;


    Double mLat, mLng;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;


    public CurrentLocation() {

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.activity_location, container, false);

        //Initialize Service
        initLocationService();

        Button getLocationButton = rootView.findViewById(R.id.getLocationButton);
        getLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProgressDialogClass.showProgress(getActivity());

                startLocationUpdates();

              /*  // instantiate the location manager, note you will need to request permissions in your manifest
                LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                // get the last know location from your location manager.

                criteria = new Criteria();
                bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true)).toString();

                //You can still do this if you like, you might get lucky:
                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    return;
                }


                Location location = locationManager.getLastKnownLocation(bestProvider);
                if (location != null) {




                    Log.e("TAG", "GPS is on");
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    // Toast.makeText(getActivity(), "latitude:" + latitude + " longitude:" + longitude, Toast.LENGTH_SHORT).show();

                    Fragment fragmentName = null;
                    Fragment RestaurantFragment = new MallFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString("lat", "" + latitude);
                    bundle.putString("lng", "" + longitude);
                    RestaurantFragment.setArguments(bundle);

                    fragmentName = RestaurantFragment;
                    replaceFragment(fragmentName);

                    // Toast.makeText(getActivity(), "" + getAddress(latitude, longitude), Toast.LENGTH_SHORT).show();
                    //getNearestMall(latitude, longitude);
                }*/
            }

        });


        return rootView;
    }

    public void getNearestMall(Double latitude, Double longitude) {
        AndroidNetworking.get(ConfigURL.URL_GET_NEAREST_MALL)
                .addQueryParameter("latitude", latitude.toString())
                .addQueryParameter("longitude", longitude.toString())
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String mall_id, mall_name, mall_area_id;
                        try {
                            JSONArray jsonArray = response.getJSONArray("mall");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                Mall mall = new Mall(jsonArray.getJSONObject(i));

                                mall_id = mall.getMall_id();
                                mall_name = mall.getMall_name();
                                mall_area_id = mall.getMall_area_id();


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public String getAddress(Double lat, Double lng) {

        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        address = addresses.get(0).getAddressLine(0);

        return address;
    }


    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;


        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = false;
        try {

            fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        } catch (IllegalStateException ignored) {
            // There's no way to avoid getting this if saveInstanceState has already been called.
        }
        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commitAllowingStateLoss();
        }

    }


    public void initLocationService() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        createLocationCallback();
        createLocationRequest();
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                mCurrentLocation = locationResult.getLastLocation();
                updateLocationUI();
            }
        };
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            mLat = mCurrentLocation.getLatitude();
            mLng = mCurrentLocation.getLongitude();

            ProgressDialogClass.hideProgress();
            stopLocationUpdates();

            Log.e("TAG", "GPS is on");

            Fragment fragmentName = null;
            Fragment RestaurantFragment = new MallFragment();

            Bundle bundle = new Bundle();
            bundle.putString("lat", "" + mLat);
            bundle.putString("lng", "" + mLng);
            RestaurantFragment.setArguments(bundle);

            fragmentName = RestaurantFragment;
            replaceFragment(fragmentName);

            //Toast.makeText(getActivity(), "Lat: " + mCurrentLocation.getLatitude() + "Lng: " + mCurrentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
        }
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, null);
        updateUI();

    }

    private void updateUI() {
        updateLocationUI();
    }


}
