package com.example.faraz.myapplication.Notification;


import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Utills.ConfigURL;

import org.json.JSONObject;

public class SendRegistrationTokenFCM {

    public static void sendRegistrationToServer(Context context, String token) {

        //Toast.makeText(this,""+username+""+password,Toast.LENGTH_LONG).show();
        AndroidNetworking.post(ConfigURL.URL_UPDATE_FCM_KEY)
                .addBodyParameter("cust_name", ConfigURL.getLoginUserName(context))
                .addBodyParameter("cust_fcm", token)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }

                    @Override
                    public void onError(ANError error) {
                    }
                });

    }
}
