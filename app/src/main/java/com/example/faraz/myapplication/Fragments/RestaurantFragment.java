package com.example.faraz.myapplication.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Models.Restaurant;
import com.example.faraz.myapplication.Adapters.AdapterRestaurant;
import com.example.faraz.myapplication.R;

import com.example.faraz.myapplication.Utills.ConfigURL;
import com.example.faraz.myapplication.Utills.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RestaurantFragment extends Fragment{


    ArrayList<Restaurant> data = new ArrayList<>();
    RecyclerView rv;
    String get_mall_id;

    public RestaurantFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_restaurant, container, false);
        rv = (RecyclerView) rootView.findViewById(R.id.rv_restaurant);
        rv.setHasFixedSize(true);

        get_mall_id = getArguments().getString("mall_id");

        if (get_mall_id != null) {

            ProgressDialogClass.showProgress(getActivity());

            getNearestMall(get_mall_id);
        }
        return rootView;
    }

    public void getNearestMall(String mall_id) {

        data.clear();
        AndroidNetworking.get(ConfigURL.URL_GET_RESTAURANT)
                .addQueryParameter("mallId", mall_id)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        ProgressDialogClass.hideProgress();


                        String res_id, res_name, res_mall_name;
                        try {
                            JSONArray jsonArray = response.getJSONArray("restaurant");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                Restaurant mall = new Restaurant(jsonArray.getJSONObject(i));

                                res_id = mall.getRes_id();
                                res_name = mall.getRes_name();
                                res_mall_name = mall.getRes_mall_name();
                                Restaurant mall1 = new Restaurant(res_id, res_name, res_mall_name);
                                data.add(mall1);


                                AdapterRestaurant adapter = new AdapterRestaurant(getActivity(), data);
                                rv.setAdapter(adapter);

                                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                                rv.setLayoutManager(llm);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.hideProgress();

                    }
                });
    }

/*
    Fragment fragmentName = null;
    Fragment RestaurantFragment = new RestaurantFragment();


    Bundle bundle=new Bundle();
                    bundle.putString("lat", ""+latitude);
                    RestaurantFragment.setArguments(bundle);

    fragmentName = RestaurantFragment;
    replaceFragment(fragmentName);*/



}