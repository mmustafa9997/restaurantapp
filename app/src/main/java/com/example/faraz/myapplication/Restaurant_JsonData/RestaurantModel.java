package com.example.faraz.myapplication.Restaurant_JsonData;

/**
 * Created by Muhammad Faraz on 25/03/2018.
 */

public class RestaurantModel
{

    private String resName,resId;

    public RestaurantModel(String resId, String resName){

        setResName(resName);
        setResId(resId);
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }
}
