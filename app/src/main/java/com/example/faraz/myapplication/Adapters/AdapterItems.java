package com.example.faraz.myapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Models.Items;
import com.example.faraz.myapplication.R;
import com.example.faraz.myapplication.Utills.ConfigURL;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

public class AdapterItems extends RecyclerView.Adapter<AdapterItems.MyViewHolder> {

    int count = 0;
    private Context acontext;
    private ArrayList<Items> arrayList;

    public AdapterItems(Context context, ArrayList<Items> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterItems.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_items, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Items current = arrayList.get(position);
        holder.tv_deal_name.setText("Name : " + current.getItem_name());
        holder.deal_description.setText("Description : " + current.getItem_description());
        holder.deal_price.setText("Price : " + current.getItem_price());
        holder.tv_qty.setText("" + current.quantity);
        Picasso.with(acontext).load(current.getItem_images()).placeholder(R.drawable.ic_image_loding_background).into(holder.img_deals);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void sendData(String cust_id, String item_id, String deal_id, String qty, String book_id, String order_id) {
        AndroidNetworking.post(ConfigURL.URL_POST_CART)
                .addBodyParameter("cust_id", cust_id)
                .addBodyParameter("item_id", item_id)
                .addBodyParameter("deal_id", deal_id)
                .addBodyParameter("order_id", order_id)
                .addBodyParameter("qty", qty)
                .addBodyParameter("booking_id", book_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public void sendUpdatedData(String item_id, String deal_id, String qty, String book_id) {
        AndroidNetworking.post(ConfigURL.URL_POST_UPDATE_CART)
                .addBodyParameter("item_id", item_id)
                .addBodyParameter("deal_id", deal_id)
                .addBodyParameter("qty", qty)
                .addBodyParameter("booking_id", book_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView mCardView;
        public ImageView img_sub, img_add, img_deals;
        public TextView tv_deal_name, deal_description, deal_price, tv_qty;
        String item_id, deal_id, qty, booking_id, order_id;

        public MyViewHolder(View v) {
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view_items);
            tv_deal_name = (TextView) v.findViewById(R.id.tv_deal_name);
            deal_description = (TextView) v.findViewById(R.id.item_description);
            deal_price = (TextView) v.findViewById(R.id.item_price);
            tv_qty = (TextView) v.findViewById(R.id.tv_qty);
            img_add = v.findViewById(R.id.img_add);
            img_sub = v.findViewById(R.id.img_sub);
            img_deals = v.findViewById(R.id.img_deals);


            img_sub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (count == 0)
                        return;
                    count = count - 1;
                    Items current = arrayList.get(getLayoutPosition());
                    item_id = current.getItem_id();
                    booking_id = String.valueOf(current.getItem_book_no());

                    sendUpdatedData(item_id, "0", String.valueOf(count), booking_id);

                    arrayList.get(getLayoutPosition()).quantity -= 1;
                    notifyDataSetChanged();

                }
            });
            img_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    count = count + 1;
                    Items current = arrayList.get(getLayoutPosition());
                    item_id = current.getItem_id();
                    booking_id = String.valueOf(current.getItem_book_no());
                    order_id = current.getOrder_id();
                    if (count == 1)
                        sendData(ConfigURL.getLoginUserName(acontext), item_id, "0", String.valueOf(count), booking_id, order_id);
                    else
                        sendUpdatedData(item_id, "0", String.valueOf(count), booking_id);

                    arrayList.get(getLayoutPosition()).quantity += 1;
                    notifyDataSetChanged();


                }
            });
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });
        }

    }

}