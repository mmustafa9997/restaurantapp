package com.example.faraz.myapplication.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Maryam on 7/1/2018.
 */

public class Restaurant {
    String res_id ;
    String res_name ;
    String res_mall_name ;
    String res_image ;

    public Restaurant(String res_id, String res_name, String res_mall_name) {
        this.res_id = res_id;
        this.res_name = res_name;
        this.res_mall_name = res_mall_name;
    }

    public String getRes_id() {
        return res_id;
    }

    public void setRes_id(String res_id) {
        this.res_id = res_id;
    }

    public String getRes_name() {
        return res_name;
    }

    public void setRes_name(String res_name) {
        this.res_name = res_name;
    }

    public String getRes_mall_name() {
        return res_mall_name;
    }

    public void setRes_mall_name(String res_mall_name) {
        this.res_mall_name = res_mall_name;
    }

    public String getRes_image() {
        return res_image;
    }

    public void setRes_image(String res_image) {
        this.res_image = res_image;
    }

    public Restaurant(JSONObject jsonObject) {
        try {
            this.res_id = jsonObject.getString("restaurantId");
            this.res_name = jsonObject.getString("restaurantName");
            this.res_mall_name = jsonObject.getString("mallName");
            //this.res_image = jsonObject.getString("mallAreaId");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
