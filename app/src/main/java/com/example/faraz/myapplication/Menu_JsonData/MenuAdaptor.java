package com.example.faraz.myapplication.Menu_JsonData;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.faraz.myapplication.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class MenuAdaptor extends ArrayAdapter {

    private List<Menu> MenuList;
    private LayoutInflater inflater;

    public MenuAdaptor(@NonNull Context context, int resource, @NonNull List<Menu> objects) {
        super(context, resource, objects);

        MenuList = objects;

        inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.menu_layout,parent,false);
        }

        TextView item_names, item_prices, item_id;
        ImageView item_image;
        String imgurl = "http://retial-profits.000webhostapp.com/html/images/items/";

        item_names = convertView.findViewById(R.id.item_name);
        item_prices = convertView.findViewById(R.id.item_price);
        item_image = convertView.findViewById(R.id.item_img);
        item_id = convertView.findViewById(R.id.id_tag);

        item_id.setText(MenuList.get(position).getItem_id());
        item_names.setText(MenuList.get(position).getItem_name());
        item_prices.setText("PRICE: "+MenuList.get(position).getItem_price());
        String img_name = imgurl+MenuList.get(position).getItem_img();
        ImageLoader.getInstance().displayImage(img_name, item_image);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (Integer) view.getTag();
                // Access the row position here to get the correct data item
                //User user = getItem(position);
                // Do what you want here...
                Log.e("taha", ""+position);
            }
        });

        return convertView;
    }

}
