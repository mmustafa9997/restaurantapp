package com.example.faraz.myapplication.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Maryam on 6/30/2018.
 */

public class PendingOrders {
    String nearest_mall ;
    String order_status ;
    String order_restaurant ;

    public PendingOrders(String nearest_mall, String order_status, String order_restaurant) {
        this.nearest_mall = nearest_mall;
        this.order_status = order_status;
        this.order_restaurant = order_restaurant;
    }

    public PendingOrders(JSONObject jsonObject) {
        try {
            this.nearest_mall = jsonObject.getString("mallName");
            this.order_restaurant = jsonObject.getString("orderStatus");
            this.order_status = jsonObject.getString("orderStatus");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getNearest_mall() {
        return nearest_mall;
    }

    public void setNearest_mall(String nearest_mall) {
        this.nearest_mall = nearest_mall;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_restaurant() {
        return order_restaurant;
    }

    public void setOrder_restaurant(String order_restaurant) {
        this.order_restaurant = order_restaurant;
    }
}
