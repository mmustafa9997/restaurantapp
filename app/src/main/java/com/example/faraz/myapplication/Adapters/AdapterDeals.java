package com.example.faraz.myapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.faraz.myapplication.Models.Deals;
import com.example.faraz.myapplication.R;
import com.example.faraz.myapplication.Utills.ConfigURL;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

public class
AdapterDeals extends RecyclerView.Adapter<AdapterDeals.MyViewHolder> {

    int count = 0;
    private Context acontext;
    private ArrayList<Deals> arrayList;


    public AdapterDeals(Context context, ArrayList<Deals> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterDeals.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_deals, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Deals current = arrayList.get(position);
        holder.tv_deal_name.setText("Deal : "+current.getDeal_name());
        holder.deal_description.setText("Desciption : "+current.getDeal_description());
        holder.deal_price.setText("Price : "+current.getDeal_price());
        holder.tv_qty.setText("" + current.quantity);
        Picasso.with(acontext).load(current.getDeal_images()).placeholder(R.drawable.ic_image_loding_background).into(holder.img_deals);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void sendData(String cust_id, String item_id, String deal_id, final String qty, String book_id , String order_id) {
        AndroidNetworking.post(ConfigURL.URL_POST_CART)
                .addBodyParameter("cust_id", cust_id)
                .addBodyParameter("item_id", item_id)
                .addBodyParameter("deal_id", deal_id)
                .addBodyParameter("order_id", order_id)
                .addBodyParameter("qty", qty)
                .addBodyParameter("booking_id", book_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response

                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public void sendUpdatedData(String item_id, String deal_id, String qty, String book_id) {
        AndroidNetworking.post(ConfigURL.URL_POST_UPDATE_CART)
                .addBodyParameter("item_id", item_id)
                .addBodyParameter("deal_id", deal_id)
                .addBodyParameter("qty", qty)
                .addBodyParameter("booking_id", book_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView mCardView;
        public ImageView img_sub, img_add, img_deals;
        public TextView tv_deal_name, deal_description, deal_price;
        public TextView tv_qty;
        String item_id, deal_id, qty, booking_id, order_id;

        public MyViewHolder(View v) {
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view_deals);
            tv_deal_name = (TextView) v.findViewById(R.id.tv_deal_name);
            deal_description = (TextView) v.findViewById(R.id.deal_description);
            deal_price = (TextView) v.findViewById(R.id.deal_price);
            tv_qty = (TextView) v.findViewById(R.id.tv_qty);
            img_add = v.findViewById(R.id.img_add);
            img_sub = v.findViewById(R.id.img_sub);
            img_deals = v.findViewById(R.id.img_deals);

            img_sub.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (count == 0)
                        return;
                    count = count - 1;
                    Deals current = arrayList.get(getLayoutPosition());
                    item_id = current.getDeal_id();
                    booking_id = String.valueOf(current.getDeal_booking_id());

                    sendUpdatedData("0", item_id, String.valueOf(count), booking_id);

                    arrayList.get(getLayoutPosition()).quantity -= 1;
                    notifyDataSetChanged();
                    //   tv_qty.setText(String.valueOf(current.quantity));

                }
            });
            img_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    count = count + 1;
                    Deals current = arrayList.get(getLayoutPosition());
                    item_id = current.getDeal_id();
                    booking_id = String.valueOf(current.getDeal_booking_id());
                    order_id = current.getOrder_id();
                    if (count == 1)
                        sendData(ConfigURL.getLoginUserName(acontext), "0", item_id, String.valueOf(count), booking_id, order_id);
                    else
                        sendUpdatedData("0", item_id, String.valueOf(count), booking_id);
                    arrayList.get(getLayoutPosition()).quantity += 1;
                    notifyDataSetChanged();
                    //tv_qty.setText(String.valueOf(current.quantity));

                }
            });

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });
        }

    }

}