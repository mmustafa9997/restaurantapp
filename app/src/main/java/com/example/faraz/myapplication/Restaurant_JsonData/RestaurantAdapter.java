package com.example.faraz.myapplication.Restaurant_JsonData;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.faraz.myapplication.R;

import java.util.List;

/**
 * Created by Muhammad Faraz on 30/03/2018.
 */

public class RestaurantAdapter extends ArrayAdapter{


    private List<RestaurantModel> restaurantList;
    private LayoutInflater inflater;

    public RestaurantAdapter(@NonNull Context context, int resource, @NonNull List<RestaurantModel> objects) {
        super(context, resource, objects);

        restaurantList = objects;

        inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null)
        {
            convertView = inflater.inflate(R.layout.res_layout, parent,false);
        }

        TextView ResName;
        TextView ResId;
        ImageView ResImg;

        ResName = convertView.findViewById(R.id.res_name);
        ResId = convertView.findViewById(R.id.res_id);
        ResName.setText(restaurantList.get(position).getResName());
        ResId.setText(restaurantList.get(position).getResId());
        return convertView;
    }
}
