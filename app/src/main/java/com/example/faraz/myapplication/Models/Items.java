package com.example.faraz.myapplication.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Maryam on 7/1/2018.
 */

public class Items {
    String item_id;
    String item_name;
    String item_price;
    String item_description;
    String item_images;
    String item_res_name;
    int item_book_no;
    String order_id ;
    public int quantity;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getItem_book_no() {
        return item_book_no;
    }

    public void setItem_book_no(int item_book_no) {
        this.item_book_no = item_book_no;
    }

    public Items(String item_id, String item_name, String item_price, String item_description, String item_images, String item_res_name, int item_book_no , String order_id) {
        this.item_id = item_id;
        this.item_name = item_name;
        this.item_price = item_price;
        this.item_description = item_description;
        this.item_images = item_images;
        this.item_res_name = item_res_name;
        this.item_book_no = item_book_no;
        this.order_id = order_id ;
        quantity = 0;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_price() {
        return item_price;
    }

    public void setItem_price(String item_price) {
        this.item_price = item_price;
    }

    public String getItem_description() {
        return item_description;
    }

    public void setItem_description(String item_description) {
        this.item_description = item_description;
    }

    public String getItem_images() {
        return item_images;
    }

    public void setItem_images(String item_images) {
        this.item_images = item_images;
    }

    public String getItem_res_name() {
        return item_res_name;
    }

    public void setItem_res_name(String item_res_name) {
        this.item_res_name = item_res_name;
    }


    public Items(JSONObject jsonObject) {
        try {
            this.item_id = jsonObject.getString("itemId");
            this.item_name = jsonObject.getString("itemName");
            this.item_price = jsonObject.getString("itemPrice");
            this.item_description = jsonObject.getString("itemDescription");
            this.item_images = jsonObject.getString("itemImage");
            this.item_res_name = jsonObject.getString("restaurantName");
            //this.res_image = jsonObject.getString("mallAreaId");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
