package com.example.faraz.myapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.faraz.myapplication.Models.CompletedOrders;
import com.example.faraz.myapplication.R;

import java.util.ArrayList;

public class AdapterOrderCompleted extends RecyclerView.Adapter<AdapterOrderCompleted.MyViewHolder> {

    private Context acontext;
    private ArrayList<CompletedOrders> arrayList ;

    public AdapterOrderCompleted(Context context, ArrayList<CompletedOrders> arrayList) {
        this.arrayList = arrayList;
        acontext = context;
    }

    @Override
    public AdapterOrderCompleted.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_order_cancel, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){
        CompletedOrders current = arrayList.get(position);
        holder.tv_completed_order_area.setText(current.getArea());
        holder.tv_completed_order_nearest_mall.setText(current.getOrder_mall());
        holder.tv_completed_order_price.setText(current.getOrder_price());
        holder.tv_completed_order_restaurants.setText(current.getRestaurant());
    }

    @Override
    public int getItemCount() {return arrayList.size(); }

    public  class MyViewHolder extends RecyclerView.ViewHolder{

        public CardView mCardView;
        public TextView tv_completed_order_area, tv_completed_order_nearest_mall, tv_completed_order_price, tv_completed_order_restaurants;

        public MyViewHolder(View v){
            super(v);
            mCardView = (CardView) v.findViewById(R.id.card_view_order_cancel);
            tv_completed_order_area = (TextView) v.findViewById(R.id.tv_completed_order_area);
            tv_completed_order_nearest_mall = (TextView) v.findViewById(R.id.tv_completed_order_nearest_mall);
            tv_completed_order_price = (TextView) v.findViewById(R.id.tv_completed_order_price);
            tv_completed_order_restaurants = (TextView) v.findViewById(R.id.tv_completed_order_restaurant);
        }

    }

}