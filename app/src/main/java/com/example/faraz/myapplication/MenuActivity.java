package com.example.faraz.myapplication;

import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.faraz.myapplication.Menu_JsonData.Menu;
import com.example.faraz.myapplication.Menu_JsonData.MenuAdaptor;
import com.example.faraz.myapplication.Utills.ConfigURL;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class MenuActivity extends AppCompatActivity {

    String response,line,i_name,i_image,r_id;
    String i_price,i_id;
    ListView lv;
    List<Menu> menuList;
    JSONObject jobject;
    JSONArray jarray;
    MenuAdaptor menuadaptor;
    Button cart;
    ArrayList array;
    ListView list_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        lv = findViewById(R.id.menulist);
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
           .cacheInMemory(true)
                .cacheOnDisk(true)
           .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
           .defaultDisplayImageOptions(defaultOptions)
           .build();
        ImageLoader.getInstance().init(config);

        Menu_Fetch mf = new Menu_Fetch();
        mf.execute();


        //list_view = findViewById(R.id.menulist);


//                list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        String item = ((TextView) view.findViewById(R.id.id_tag)).getText().toString();
//                        Toast.makeText(MenuActivity.this, item, Toast.LENGTH_SHORT).show();
//                        array = new ArrayList();
//                        array.add(item);
//
//                    }
//                });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                Log.e("############","Items " + arg2 );
            }

        });



    }



    class Menu_Fetch extends AsyncTask<String,Object,List<Menu>>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected List<Menu> doInBackground(String... strings) {
            String link = ConfigURL.URL_GET_MENU_ITEM;
            r_id = getIntent().getStringExtra("rest_id");
            try {
                Res_Based_Menu(link,r_id);
                conversion(response);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return menuList;
        }

        @Override
        protected void onPostExecute(List<Menu> menus)
        {
            menuadaptor = new MenuAdaptor(getApplicationContext(),R.layout.menu_layout,menus);
            lv.setAdapter(menuadaptor);
        }
    }


    public String Res_Based_Menu(String api,String res_id) throws IOException {

        String r_id = URLEncoder.encode("res","UTF-8") +"="+ URLEncoder.encode(res_id,"UTF-8");

        URL url = new URL(api);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        OutputStream os = connection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
        writer.write(r_id);
        writer.flush();
        writer.close();
        os.close();

        InputStream inputStream = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
        response = "";
        line = "";

        while((line = reader.readLine()) != null)
        {
            response += line;
        }
        reader.close();
        inputStream.close();
        connection.disconnect();

        return response;
    }

    public List<Menu> conversion(String json_data) throws JSONException {


        menuList = new ArrayList<>();
        jobject = new JSONObject(json_data);
        jarray = jobject.getJSONArray("items");
        int count = 0;

        while(count < jarray.length())
        {
            JSONObject object = jarray.getJSONObject(count);

            i_id = object.getString("item_id");
            i_name = object.getString("item_name");
            i_price = object.getString("item_price");
            i_image = object.getString("item_image");

            Menu menu = new Menu(i_id,i_name,i_price,i_image);
            menuList.add(menu);
            count++;

        }

        return menuList;
    }

}

